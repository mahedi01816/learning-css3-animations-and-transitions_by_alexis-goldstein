window.onload = setup;

function setup() {
    var dismiss = document.getElementById("dismiss");
    dismiss.onclick = hide;
}

function hide() {
    document.getElementById("unsupported-browser").className = "hide";
}